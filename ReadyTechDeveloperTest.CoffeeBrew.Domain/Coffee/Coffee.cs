﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadyTechDeveloperTest.CoffeeBrew.Domain.Coffee
{
    public class Coffee
    {
        public Guid Id { get; set; }
        public string Message { get; set; }
        public DateTime Prepared { get; set; }
    }
}
