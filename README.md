
# ReadyTechCodingChallenge

Coding Challenge

##  .NET Core API

This api is a connect to the imaginary coffee machine and also interact with external API  '[https://openweathermap.org/api](https://openweathermap.org/api)' notify users by addressing  below statuses.

1. 200 OK 
2. Service Unavailable
3. 418 I'm a Tea Pot. 


# To run this application on local machine:

 - Clone this solution and run using VS 2019.
 - Application configured to be running on - [http://localhost:60392/](http://localhost:60392/) 
 - Once running the application user will be redirected to swagger documentation  - [ http://localhost:60392/swagger/index.html]( http://localhost:60392/swagger/index.html)
    - Integrated swagger documentation to the api 
	
	![ImgName](https://bitbucket.org/mlin16/readytechdevelopertest/raw/extracredit/ReadyTechDeveloperTest.CoffeeBrew/Images/SwaggerDocumentation-RepoReflink.png)
	
	 - Xunit is used for testing.
 
    ![ImgName](https://bitbucket.org/mlin16/readytechdevelopertest/raw/extracredit/ReadyTechDeveloperTest.CoffeeBrew/Images/TestCases-RepoRefLink.png)

## Acknowledgments

  - The people of ReadyTech for the opportunity.
