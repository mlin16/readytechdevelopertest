﻿using ReadyTechDeveloperTest.CoffeeBrew.ApplicationContracts.Coffee;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadyTechDeveloperTest.CoffeeBrew.Application.Coffee
{
    /// <summary>
    /// I can Adopt Adapter or Decorator pattern  as per the requirement it said to be connected to a imaginary coffee machine but not machines.
    /// So, I preferred to keep it simple with a decorator pattern through which we can add responsibilites to object dynamically.
    /// Currently in the system we only have a simple coffee service. Later point of time it can be MilkCoffee , WhipCoffee ☕ ,...
    /// </summary>
    public class CoffeeAppService : ICoffeeAppService
    {
        private static string _format= "yyyy-MM-ddTHH:mm:sszzz";
        private static CultureInfo _culture = CultureInfo.InvariantCulture;

        public uint GetCost()
        {
            return 5;
        }

        public CoffeeDto GetStatus()
        {
            var dateTime = DateTime.Now.ToString(_format, _culture);

            return _ = new CoffeeDto
            {
                Id=Guid.NewGuid(),
                Message = "Your piping hot coffee is ready",
                Prepared = DateTime.Parse(dateTime)
            };
        }
    }
}
