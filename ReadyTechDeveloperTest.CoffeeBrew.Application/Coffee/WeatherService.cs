﻿using FluentResults;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ReadyTechDeveloperTest.CoffeeBrew.Application.Common.Settings;
using ReadyTechDeveloperTest.CoffeeBrew.ApplicationContracts.Coffee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ReadyTechDeveloperTest.CoffeeBrew.Application.Coffee
{
    public class WeatherService : IWeatherService
    {
        private readonly HttpClient _httpClient;
        private readonly string _serviceUrl;
        private readonly ILoggerManager _logger;

        public WeatherService(HttpClient httpClient,  ILoggerManager logger  ,IOptions<AppSettings> settings)
        {
            _httpClient = httpClient;
            _logger = logger;
            _serviceUrl = settings?.Value?.ServiceUrl ?? throw new ArgumentNullException(nameof(settings));

        }
        public async Task<Result<double>> GetWeather()
        {
            try
            {
                //TODO : Service url is configured in appsettings. It can be made a base url and parameterized accordingly to inject thru Iconfiguration.
                var content = await _httpClient.GetAsync(_serviceUrl);
                var jsonContent = await content.Content.ReadAsStringAsync();

                var rawWeather = JsonConvert.DeserializeObject<OpenWeatherDto>(jsonContent);
                return Result.Ok(Convert.ToDouble(rawWeather.Main.Temp));

            }
            catch (Exception exception)
            {
                _logger.LogError(exception.Message);

                return Result.Fail<double>(new Error(exception.Message));
            }
        }
    }
}
