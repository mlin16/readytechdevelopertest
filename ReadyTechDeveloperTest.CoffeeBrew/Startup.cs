using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using ReadyTechDeveloperTest.CoffeeBrew.Application.Coffee;
using ReadyTechDeveloperTest.CoffeeBrew.Application.Common.Settings;
using ReadyTechDeveloperTest.CoffeeBrew.ApplicationContracts.Coffee;
using ReadyTechDeveloperTest.CoffeeBrew.Filters;
using ReadyTechDeveloperTest.ExceptionMiddlewareExtensionscs;

namespace ReadyTechDeveloperTest.CoffeeBrew
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("EnableCors", builder =>
                {
                    builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
                });
            });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<ILoggerManager, LoggerManager>();
            services.AddTransient<ICoffeeAppService, CoffeeAppService>();
            services.AddTransient<IClock, ClockService>();

            // Add AppSettings
            services.Configure<AppSettings>(Configuration);
            // Add HTTP Services
            services.AddHttpClient<IWeatherService, WeatherService>();

            services.AddControllers();

            services
             .AddMvc(options =>
             {
                 options.InputFormatters.RemoveType<Microsoft.AspNetCore.Mvc.Formatters.SystemTextJsonInputFormatter>();
                 options.OutputFormatters.RemoveType<Microsoft.AspNetCore.Mvc.Formatters.SystemTextJsonOutputFormatter>();
             })

             .AddNewtonsoftJson(opts =>
             {
                 opts.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                 opts.SerializerSettings.Converters.Add(new StringEnumConverter(new CamelCaseNamingStrategy()));
             })
               .AddXmlSerializerFormatters();

            services
                .AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("1.0.0", new OpenApiInfo
                    {
                        Version = "1.0.0",
                        Title = "SmartCoffee API",
                        Description = "This is a Smartcoffee api implementation developer test conducted by ReadyTech  to evaluate candiadate technical skills.\n",
                        Contact = new OpenApiContact()
                        {
                            Name = "Swagger Codegen Contributors",
                            Url = new Uri("https://github.com/swagger-api/swagger-codegen"),
                            Email = "lingalamanojreddy@gmail.com"
                        },
                        TermsOfService = new Uri("https://www.readytech.com.au//")
                    });
                    c.CustomSchemaIds(type => type.FullName);

                    // Set the comments path for the Swagger JSON and UI.
                    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                    c.IncludeXmlComments(xmlPath);
                    // Sets the basePath property in the Swagger document generated
                    c.DocumentFilter<BasePathFilter>("/api");

                    // Include DataAnnotation attributes on Controller Action parameters as Swagger validation rules (e.g required, pattern, ..)
                    // Use [ValidateModelState] on Actions to actually validate it in C# as well!
                    c.OperationFilter<GeneratePathParamsValidationFilter>();
                });

            //TODO I wanted to register polly to avoid the transitent failure issues and to make a successful automated retries in case of any failures.

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerManager logger)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            //Exception Handling
            ExceptionMiddlewareExtensions.ConfigureExceptionHandler(app, logger);
            app.UseStaticFiles();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                //TODO: Or alternatively use the original Swagger contract that's included in the static files
                c.SwaggerEndpoint("/swagger-original-coffeebrew.json", "SmartCoffee API (ASP.NET Core 3.0)");
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
