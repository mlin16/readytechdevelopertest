﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ReadyTechDeveloperTest.CoffeeBrew.ApplicationContracts.Coffee;
using ReadyTechDeveloperTest.CoffeeBrew.Attributes;
using Swashbuckle.AspNetCore.Annotations;

namespace ReadyTechDeveloperTest.CoffeeBrew.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BrewCoffeeController : ControllerBase
    {
        private readonly ICoffeeAppService _appService;
        private readonly IWeatherService _weatherService;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IClock _clock;

        public BrewCoffeeController(ICoffeeAppService appService , IWeatherService weatherService , IHttpContextAccessor contextAccessor , IClock clock)
        {
            _appService = appService;
            _weatherService = weatherService;
            _contextAccessor = contextAccessor;
            _clock = clock;
        }
        /// <summary>
        /// Used Swashbuckle nuget pkg to generate the doucmentation of the api.
        /// So, you can see SwaggerResponse decorator instead of default ProducesResponseType
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("/smartcoffee/brew-coffee")]
        [ValidateModelState]
        [SwaggerOperation("GetBrewCoffee")]
        [SwaggerResponse(statusCode: 200, type: typeof(CoffeeDto), description: "Successful operation")]
        [SwaggerResponse(statusCode: 400, type: typeof(ErrorDto), description: "Invalid input, object invalid")]
        [SwaggerResponse(statusCode: 500, type: typeof(ErrorDto), description: "Internal Server Error")]
        [SwaggerResponse(statusCode: 503, type: null, description: "Service Unavailable")]
        [SwaggerResponse(statusCode: 418, type: null, description: "I'm a teapot")]
        [RequestRateLimit(Name = "Limit Request Number", Count = 5)]
        public  async Task<IActionResult> GetBrewCoffeeAsync()
        {
            try
            {
                /*Incrementing the count in the RequestRateLimit attribute decorator class and setting the statuscode to 503.
                 * If continuous request is made for 5 times service unavailability is shown to the client . We could also use 
                 * timestamp like seconds,milliseconds to refill to avoid unavailability 
                 */
                if (_contextAccessor.HttpContext.Response.StatusCode==503)
                    return StatusCode(503,null);
            
                var temp = await _weatherService.GetWeather();

                if (temp.Value > 30)
                {
                    var changeNotification = _appService.GetStatus();
                    changeNotification.Message = "Your refreshing iced coffee is ready";
                    return StatusCode(200, changeNotification);

                }
                
                //Injected Iclock to easily mock for the tea pot test case 😎

                if ((_clock.Now.Month == 4) && (_clock.Now.Day == 1))
                    return StatusCode(418, null);

                var notifyUser = _appService.GetStatus();
                return StatusCode(200, notifyUser != null ? notifyUser : default(CoffeeDto));


                //TODO: Uncomment the next line to return response 400 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
                // return StatusCode(400, default(ErrorDto);
            }
            catch (Exception ex)
            {
                var error = new ErrorDto { Id=Guid.NewGuid(), Code=500,Detail= ex.Message};
                return StatusCode(500, error);
            }

        }
    }
}
