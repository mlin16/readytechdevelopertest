﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ReadyTechDeveloperTest.CoffeeBrew.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class RequestRateLimitAttribute : ActionFilterAttribute
    {
        public string Name { get; set; }

        public int Count { get; set; }

        private static MemoryCache Cache { get; } = new MemoryCache(new MemoryCacheOptions());
        
        private int _requestCount = 0;

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            _requestCount++;
            if (_requestCount >= Count)
            {
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.ServiceUnavailable;
            }
        }
    }
}
