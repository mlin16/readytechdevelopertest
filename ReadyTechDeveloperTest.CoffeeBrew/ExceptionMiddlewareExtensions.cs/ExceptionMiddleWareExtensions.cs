﻿using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using ReadyTechDeveloperTest.CoffeeBrew.ApplicationContracts.Coffee;

namespace ReadyTechDeveloperTest.ExceptionMiddlewareExtensionscs
{
    /// <summary>
    /// Exception middleware is used to Log all unhandled exception to log files
    /// Configured is available in nlog.config
    /// </summary>
    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureExceptionHandler(IApplicationBuilder app, ILoggerManager logger)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "application/json";

                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (contextFeature != null)
                    {
                        logger.LogError($"Something went wrong: {contextFeature.Error}");

                        await context.Response.WriteAsync(new ErrorDto()
                        {
                            Id=Guid.NewGuid(),
                            Code = context.Response.StatusCode,
                            Detail = "Internal Server Error."
                        }.ToString()); 
                    }
                });
            });
        }
    }
}
