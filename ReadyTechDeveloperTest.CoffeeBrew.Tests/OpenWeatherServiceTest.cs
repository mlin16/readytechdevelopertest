using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Moq;
using NSubstitute;
using ReadyTechDeveloperTest.CoffeeBrew.Application.Coffee;
using ReadyTechDeveloperTest.CoffeeBrew.Application.Common.Settings;
using ReadyTechDeveloperTest.CoffeeBrew.ApplicationContracts.Coffee;
using RichardSzalay.MockHttp;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace ReadyTechDeveloperTest.CoffeeBrew.Tests
{
    public class OpenWeatherServiceTest
    {
        private WeatherService _sut;
        private readonly IOptions<AppSettings> _appSettings;

        public OpenWeatherServiceTest()
        {
            _appSettings = Options.Create(new AppSettings() { ServiceUrl = "http://ServiceUrl" });
        }
        [Fact]
        public async Task ShouldBeSuccess_GetWeather()
        {
            // Arrange
            const string responseJSON = "{'coord':{'lon':144.9633,'lat':-37.814},'weather':[{'id':804,'main':'Clouds','description':'overcast clouds','icon':'04d'}]" +
                ",'base':'stations','main':{'temp':31,'feels_like':12.73,'temp_min':13,'temp_max':15.56,'pressure':1023,'humidity':94},'visibility':10000,'wind':{'speed':4.12,'deg':210},'clouds':{'all':90},'dt':1616966757," +
                "'sys':{'type':1,'id':9548,'country':'AU','sunrise':1616963455,'sunset':1617005929},'timezone':39600,'id':2158177,'name':'Melbourne','cod':200}";

            var mockHttp = new MockHttpMessageHandler();

            mockHttp.When(_appSettings.Value.ServiceUrl)
                    .Respond("application/json", responseJSON);
    

            var client = mockHttp.ToHttpClient();

            _sut = new WeatherService(
                client,
            Substitute.For<ILoggerManager>(), _appSettings);

            // Act
            var temperature = await _sut.GetWeather();
            var result = temperature.Value;

            // Assert
            Assert.True(temperature.IsSuccess);
            Assert.Equal(31, result);
        }

        [Fact]
        public async Task ShouldFail_GetWeather()
        {
            // Arrange
            var mockHttp = new MockHttpMessageHandler();

            mockHttp.When(_appSettings.Value.ServiceUrl)
                .Respond(req => new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError
                });

            var client = mockHttp.ToHttpClient();

            _sut = new WeatherService(
               client,
              Substitute.For<ILoggerManager>(), _appSettings);

            // Act
            var temperature = await _sut.GetWeather();

            // Assert
            Assert.True(temperature.IsFailed);
        }


        [Fact]
        public async Task Should_GetWeather_ConditionFailed()
        {
            // Arrange
            const string responseJSON = "{'coord':{'lon':144.9633,'lat':-37.814},'weather':[{'id':804,'main':'Clouds','description':'overcast clouds','icon':'04d'}]" +
            ",'base':'stations','main':{'temp':31,'feels_like':12.73,'temp_min':13,'temp_max':15.56,'pressure':1023,'humidity':94},'visibility':10000,'wind':{'speed':4.12,'deg':210},'clouds':{'all':90},'dt':1616966757," +
            "'sys':{'type':1,'id':9548,'country':'AU','sunrise':1616963455,'sunset':1617005929},'timezone':39600,'id':2158177,'name':'Melbourne','cod':200}";

            var mockHttp = new MockHttpMessageHandler();

            mockHttp.When(_appSettings.Value.ServiceUrl)
                    .Respond("application/json", responseJSON);

            var client = mockHttp.ToHttpClient();

            _sut = new WeatherService(
                       client,
                      Substitute.For<ILoggerManager>(), _appSettings);

            var response = await _sut.GetWeather();
            var result = response.Value;

            // Assert           

            Assert.True(response.IsSuccess);
            Assert.False(result > 31);

        }
    }
}
