﻿using System.Threading.Tasks;
using Xunit;
using System.Net;
using System.Globalization;
using System;
using ReadyTechDeveloperTest.CoffeeBrew.ApplicationContracts.Coffee;
using ReadyTechDeveloperTest.CoffeeBrew.Controllers;
using Moq;
using FluentResults;
using NSubstitute;
using ReadyTechDeveloperTest.CoffeeBrew.Application.Coffee;
using Microsoft.Extensions.Options;
using ReadyTechDeveloperTest.CoffeeBrew.Application.Common.Settings;
using RichardSzalay.MockHttp;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ReadyTechDeveloperTest.CoffeeBrew.Tests
{
    public class BrewCoffeeTest
    {
        private static string _format = "yyyy-MM-ddTHH:mm:sszzz";
        private static CultureInfo _culture = CultureInfo.InvariantCulture;
        private WeatherService _sut;
        private readonly IOptions<AppSettings> _appSettings;

        public BrewCoffeeTest()
        {
            _appSettings = Options.Create(new AppSettings() { ServiceUrl = "http://ServiceUrl" });
        }

        [Fact]
        public async Task TwoHunderedOK_GetWeather_TemperatureGreaterThan30()
        {
            //Arrange
            var request = new Mock<HttpResponse>();
            request.Setup(x => x.StatusCode).Returns(200);

            var httpContext = Mock.Of<IHttpContextAccessor>(_ => _.HttpContext.Response == request.Object);

            var dateTime = DateTime.Now.ToString(_format, _culture);



            const string responseJSON = "{'coord':{'lon':144.9633,'lat':-37.814},'weather':[{'id':804,'main':'Clouds','description':'overcast clouds','icon':'04d'}]" +
                ",'base':'stations','main':{'temp':31,'feels_like':12.73,'temp_min':13,'temp_max':15.56,'pressure':1023,'humidity':94},'visibility':10000,'wind':{'speed':4.12,'deg':210},'clouds':{'all':90},'dt':1616966757," +
                "'sys':{'type':1,'id':9548,'country':'AU','sunrise':1616963455,'sunset':1617005929},'timezone':39600,'id':2158177,'name':'Melbourne','cod':200}";

            var sutWeatherService = OpenMap(responseJSON);

            var coffee = new CoffeeDto
            {
                Id = Guid.NewGuid(),
                Message = "Your piping hot coffee is ready",
                Prepared = DateTime.Parse(dateTime)
            };

            //Act

            var brewServiceMock = new Mock<ICoffeeAppService>();
            brewServiceMock.Setup(p => p.GetStatus()).Returns(coffee);

            var clockMock = new Mock<IClock>();
            clockMock.Setup(p => p.Kind).Returns(DateTimeKind.Local);

            var weatherServiceMock = new Mock<IWeatherService>();
            weatherServiceMock.Setup(s => s.GetWeather()).Returns(sutWeatherService.GetWeather());

            var controller = new BrewCoffeeController(brewServiceMock.Object, weatherServiceMock.Object, httpContext, clockMock.Object);

            var result = await controller.GetBrewCoffeeAsync() as ObjectResult;
            var actualResult = result.Value;

            //Assert
            Assert.Equal(HttpStatusCode.OK, (HttpStatusCode)result.StatusCode);
            Assert.Equal("Your refreshing iced coffee is ready", ((CoffeeDto)actualResult).Message);
        }


        [Fact]
        public async Task Every5thCall_GetServiceUnavailable()
        {
            //Arrange
            var request = new Mock<HttpResponse>();
            request.Setup(x => x.StatusCode).Returns(503);

            var httpContext = Mock.Of<IHttpContextAccessor>(_ => _.HttpContext.Response == request.Object);

            var dateTime = DateTime.Now.ToString(_format, _culture);

            const string responseJSON = "{'coord':{'lon':144.9633,'lat':-37.814},'weather':[{'id':804,'main':'Clouds','description':'overcast clouds','icon':'04d'}]" +
                ",'base':'stations','main':{'temp':29,'feels_like':12.73,'temp_min':13,'temp_max':15.56,'pressure':1023,'humidity':94},'visibility':10000,'wind':{'speed':4.12,'deg':210},'clouds':{'all':90},'dt':1616966757," +
                "'sys':{'type':1,'id':9548,'country':'AU','sunrise':1616963455,'sunset':1617005929},'timezone':39600,'id':2158177,'name':'Melbourne','cod':200}";

            var sutWeatherService = OpenMap(responseJSON);

            var coffee = new CoffeeDto
            {
                Id = Guid.NewGuid(),
                Message = "Your piping hot coffee is ready",
                Prepared = DateTime.Parse(dateTime)
            };

            //Act

            var brewServiceMock = new Mock<ICoffeeAppService>();
            brewServiceMock.Setup(p => p.GetStatus()).Returns(coffee);

            var weatherServiceMock = new Mock<IWeatherService>();
            weatherServiceMock.Setup(s => s.GetWeather()).Returns(sutWeatherService.GetWeather());
            var clockMock = new Mock<IClock>();
            clockMock.Setup(p => p.Now).Returns(coffee.Prepared);

            var controller = new BrewCoffeeController(brewServiceMock.Object, weatherServiceMock.Object, httpContext, clockMock.Object);

            var result = await controller.GetBrewCoffeeAsync() as ObjectResult;

            //Assert
            Assert.Equal(HttpStatusCode.ServiceUnavailable, (HttpStatusCode)result.StatusCode);
        }

        [Fact]
        public async Task April1st_GetTeamPot()
        {
            //Arrange
            var request = new Mock<HttpResponse>();
            request.Setup(x => x.StatusCode).Returns(200);

            var httpContext = Mock.Of<IHttpContextAccessor>(_ => _.HttpContext.Response == request.Object);

            var dateTime = "2021-04-1T13:47:54+11:00";

            const string responseJSON = "{'coord':{'lon':144.9633,'lat':-37.814},'weather':[{'id':804,'main':'Clouds','description':'overcast clouds','icon':'04d'}]" +
                ",'base':'stations','main':{'temp':29,'feels_like':12.73,'temp_min':13,'temp_max':15.56,'pressure':1023,'humidity':94},'visibility':10000,'wind':{'speed':4.12,'deg':210},'clouds':{'all':90},'dt':1616966757," +
                "'sys':{'type':1,'id':9548,'country':'AU','sunrise':1616963455,'sunset':1617005929},'timezone':39600,'id':2158177,'name':'Melbourne','cod':200}";

            var sutWeatherService = OpenMap(responseJSON);

            var coffee = new CoffeeDto
            {
                Id = Guid.NewGuid(),
                Message = "Your piping hot coffee is ready",
                Prepared = DateTime.Parse(dateTime)
            };

            //Act

            var brewServiceMock = new Mock<ICoffeeAppService>();
            brewServiceMock.Setup(p => p.GetStatus()).Returns(coffee);

            var weatherServiceMock = new Mock<IWeatherService>();
            weatherServiceMock.Setup(s => s.GetWeather()).Returns(sutWeatherService.GetWeather());
            var clockMock = new Mock<IClock>();
            clockMock.Setup(p => p.Now).Returns(coffee.Prepared);

            var controller = new BrewCoffeeController(brewServiceMock.Object, weatherServiceMock.Object, httpContext, clockMock.Object);

            var result = await controller.GetBrewCoffeeAsync() as ObjectResult;

            //Assert
            Assert.Equal(418, result.StatusCode);
        }

        [Fact]
        public async Task TwoHunderedOK_GetWeather_TemperatureLessThan30()
        {
            //Arrange
            var request = new Mock<HttpResponse>();
            request.Setup(x => x.StatusCode).Returns(200);

            var httpContext = Mock.Of<IHttpContextAccessor>(_ => _.HttpContext.Response == request.Object);

            var dateTime = DateTime.Now.ToString(_format, _culture);

            const string responseJSON = "{'coord':{'lon':144.9633,'lat':-37.814},'weather':[{'id':804,'main':'Clouds','description':'overcast clouds','icon':'04d'}]" +
                ",'base':'stations','main':{'temp':29,'feels_like':12.73,'temp_min':13,'temp_max':15.56,'pressure':1023,'humidity':94},'visibility':10000,'wind':{'speed':4.12,'deg':210},'clouds':{'all':90},'dt':1616966757," +
                "'sys':{'type':1,'id':9548,'country':'AU','sunrise':1616963455,'sunset':1617005929},'timezone':39600,'id':2158177,'name':'Melbourne','cod':200}";

            var sutWeatherService = OpenMap(responseJSON);

            var coffee = new CoffeeDto
            {
                Id = Guid.NewGuid(),
                Message = "Your piping hot coffee is ready",
                Prepared = DateTime.Parse(dateTime)
            };

            //Act

            var brewServiceMock = new Mock<ICoffeeAppService>();
            brewServiceMock.Setup(p => p.GetStatus()).Returns(coffee);

            var weatherServiceMock = new Mock<IWeatherService>();
            weatherServiceMock.Setup(s => s.GetWeather()).Returns(sutWeatherService.GetWeather());
            var clockMock = new Mock<IClock>();
            clockMock.Setup(p => p.Now).Returns(coffee.Prepared);

            var controller = new BrewCoffeeController(brewServiceMock.Object, weatherServiceMock.Object, httpContext,clockMock.Object);

            var result = await controller.GetBrewCoffeeAsync() as ObjectResult;
            var actualResult = result.Value;

            //Assert
            Assert.Equal(HttpStatusCode.OK, (HttpStatusCode)result.StatusCode);
            Assert.Equal("Your piping hot coffee is ready", ((CoffeeDto)actualResult).Message);
        }


        private WeatherService OpenMap(string responseJSON)
        {
            var mockHttp = new MockHttpMessageHandler();

            mockHttp.When(_appSettings.Value.ServiceUrl)
                    .Respond("application/json", responseJSON);


            var client = mockHttp.ToHttpClient();

           return _sut = new WeatherService(
                client,
            Substitute.For<ILoggerManager>(), _appSettings);
        }

    }
}
