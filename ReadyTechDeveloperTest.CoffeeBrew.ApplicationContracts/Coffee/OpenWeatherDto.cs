﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadyTechDeveloperTest.CoffeeBrew.ApplicationContracts.Coffee
{
    public class OpenWeatherDto
    {
        public string Name { get; set; }

        public IEnumerable<WeatherDto> Weather { get; set; }

        public MainDto Main { get; set; }
    }
}
