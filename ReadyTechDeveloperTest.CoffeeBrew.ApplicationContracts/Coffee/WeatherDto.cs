﻿namespace ReadyTechDeveloperTest.CoffeeBrew.ApplicationContracts.Coffee
{
    public class WeatherDto
    {
        public string Main { get; set; }
        public string Description { get; set; }
    }
}