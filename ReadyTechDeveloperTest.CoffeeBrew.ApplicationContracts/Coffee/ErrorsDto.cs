﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ReadyTechDeveloperTest.CoffeeBrew.ApplicationContracts.Coffee
{
    public class ErrorsDto
    {
        [DataMember(Name = "Errors")]
        public List<ErrorDto> Errors { get; set; }
    }
}
