﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadyTechDeveloperTest.CoffeeBrew.ApplicationContracts.Coffee
{
    public interface ICoffeeAppService
    {
        uint GetCost();
        CoffeeDto GetStatus();
    }
}
